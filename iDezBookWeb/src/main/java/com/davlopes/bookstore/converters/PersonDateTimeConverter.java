package main.java.com.davlopes.bookstore.converters;

import java.util.TimeZone;

import javax.faces.convert.DateTimeConverter;
import javax.faces.convert.FacesConverter;

@FacesConverter(forClass=java.util.Date.class)
public class PersonDateTimeConverter extends DateTimeConverter {
	
	public PersonDateTimeConverter(){
		super();
		this.setTimeZone(TimeZone.getTimeZone("America/Sao_Paulo"));
	}

}
