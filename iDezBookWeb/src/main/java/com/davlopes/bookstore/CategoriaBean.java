package main.java.com.davlopes.bookstore;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.davlopes.bookstore.manager.GerenciadorDeCategorias;
import com.davlopes.bookstore.models.Categoria;

@Named
@ConversationScoped
public class CategoriaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private Conversation conversation;

	private Categoria categoria;

	@EJB
	private GerenciadorDeCategorias gerenciador;

	private List<Categoria> categorias;

	public CategoriaBean() {
		this.categoria = new Categoria();
	}

	public Categoria getCategoria() {
		return this.categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public String cadastrar() {
		this.gerenciador.cadastrar(this.categoria);

		return "cat_admin_listar.xhtml";
	}

	public String remover(Categoria c) {
		this.gerenciador.remover(c);

		return "cat_admin_listar.xhtml";
	}

	public List<Categoria> getCategorias() {
		if (this.categorias == null) {
			this.categorias = this.gerenciador.obterTodos();
		}

		return this.categorias;
	}

	public String prepararEdicao(Categoria aEditar) {
		if (this.conversation.isTransient()) {
			this.conversation.begin();
		}
		this.categoria = aEditar;

		return "cat_admin_editar?faces-redirect=true";
	}

	public String editarCategoria() {
		this.gerenciador.alterar(this.categoria);
		this.conversation.end();

		return "cat_admin_listar.xhtml";
	}

}
