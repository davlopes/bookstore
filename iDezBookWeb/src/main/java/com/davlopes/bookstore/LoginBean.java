package main.java.com.davlopes.bookstore;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Named;

import com.davlopes.bookstore.manager.GerenciadorDeUsuarios;
import com.davlopes.bookstore.models.Usuario;

@Named
@ConversationScoped
public class LoginBean implements Serializable {

	private static final long serialVersionUID = 4348022157631061555L;

	@EJB
	private GerenciadorDeUsuarios gerenciador;
	
	private String nome;
	private String senha;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public String logar() {
		for(Usuario u : gerenciador.obterTodos()) {
			if (u.getNome().equals(this.nome) &&
					u.getSenha().equals(this.senha)) {
				return "admin.xhtml";
			}
		}
		return "invalido.xhtml";
	}
	
	public boolean getExisteUsusarios() {
		return !gerenciador.obterTodos().isEmpty();
	}
	
	
}
