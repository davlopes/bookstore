package main.java.com.davlopes.bookstore;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.davlopes.bookstore.manager.GerenciadorDeLivros;
import com.davlopes.bookstore.models.Categoria;
import com.davlopes.bookstore.models.Livro;


@Named
@ConversationScoped
public class LivroBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private Livro livro;
	
	@EJB
	private GerenciadorDeLivros gerenciador;
	
	private List<Livro> livros;
	private List<Livro> lancamentos;

	@Inject
	private Conversation conversation;
	
	public LivroBean(){
		this.livro = new Livro();
		this.livro.setCategorias(new Categoria());
	}
	
	public Livro getLivro(){
		return this.livro;
	}	
	
	public void setLivro(Livro Livro){
		this.livro = Livro;
	}
	
	public String cadastrar(){
		this.gerenciador.cadastrar(this.livro);
		
		return "liv_admin_listar.xhtml";
	}
	
	public String remover(Livro l){
		this.gerenciador.remover(l);
		
		return "liv_admin_listar.xhtml";
	}
	
	public List<Livro> getLivros(){
		if(this.livros == null){
			this.livros = this.gerenciador.obterTodos();
		}
		
		return this.livros;
	}

	public List<Livro> getLancamentos() {
		if(this.lancamentos == null){
			this.lancamentos = this.gerenciador.obterLancamentos();
		}
		
		return this.lancamentos;
	}
	
	public String prepararEdicao(Livro aEditar) {
		if (this.conversation.isTransient()) {
			this.conversation.begin();
		}
		this.livro = aEditar;

		return "liv_admin_editar?faces-redirect=true";
	}
	
	public String editarLivro() {
		this.gerenciador.alterar(this.livro);
		this.conversation.end();

		return "liv_admin_listar.xhtml";
	}
	
	public void comprar(){
		System.out.println(this.livro);
	}
		
}
