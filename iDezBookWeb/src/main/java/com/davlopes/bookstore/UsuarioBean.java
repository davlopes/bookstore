package main.java.com.davlopes.bookstore;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.enterprise.context.Conversation;
import javax.enterprise.context.ConversationScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.davlopes.bookstore.manager.GerenciadorDeUsuarios;
import com.davlopes.bookstore.models.Usuario;


@Named
@ConversationScoped
public class UsuarioBean implements Serializable{

	private static final long serialVersionUID = 1L;

	private Usuario usuario;
	
	@Inject
	private Conversation conversation;
	
	@EJB
	private GerenciadorDeUsuarios gerenciador;
	
	private List<Usuario> usuarios;
	
	public UsuarioBean(){
		this.usuario = new Usuario();
	}
	
	public Usuario getUsuario(){
		return this.usuario;
	}	
	
	public void setUsuario(Usuario Usuario){
		this.usuario = Usuario;
	}
	
	public String cadastrar(){
		this.gerenciador.cadastrar(this.usuario);
		
		return "usu_admin_listar.xhtml";
	}
	
	public String remover(Usuario u){
		this.gerenciador.remover(u);
		
		return "usu_admin_listar.xhtml";
	}
	
	public List<Usuario> getUsuarios(){
		if(this.usuarios == null){
			this.usuarios = this.gerenciador.obterTodos();
		}
		
		return this.usuarios;
	}
	
	public String prepararEdicao(Usuario aEditar) {
		if (this.conversation.isTransient()) {
			this.conversation.begin();
		}
		this.usuario = aEditar;

		return "usu_admin_editar?faces-redirect=true";
	}
	
	public String editarUsuario() {
		this.gerenciador.alterar(this.usuario);
		this.conversation.end();

		return "usu_admin_listar.xhtml";
	}
	
}
