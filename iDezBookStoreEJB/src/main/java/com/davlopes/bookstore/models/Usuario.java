package com.davlopes.bookstore.models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Usuario {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	private String nome;

	@OneToOne(cascade=CascadeType.ALL)
	private Endereco endereco;
	
	private String email;
	private String senha;

	@Temporal(TemporalType.DATE)
	private Date data_nasc;
	private String sexo;
	private int tel_res;
	private int tel_cel;
	private int tel_com;
	private int cpf;

	public Usuario() {
		this.endereco = new Endereco();
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Date getData_nasc() {
		return data_nasc;
	}

	public void setData_nasc(Date data_nasc) {
		this.data_nasc = data_nasc;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public int getTel_res() {
		return tel_res;
	}

	public void setTel_res(int tel_res) {
		this.tel_res = tel_res;
	}

	public int getTel_cel() {
		return tel_cel;
	}

	public void setTel_cel(int tel_cel) {
		this.tel_cel = tel_cel;
	}

	public int getTel_com() {
		return tel_com;
	}

	public void setTel_com(int tel_com) {
		this.tel_com = tel_com;
	}

	public int getCpf() {
		return cpf;
	}

	public void setCpf(int cpf) {
		this.cpf = cpf;
	}

}
