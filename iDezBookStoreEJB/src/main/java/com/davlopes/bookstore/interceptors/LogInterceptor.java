package com.davlopes.bookstore.interceptors;

import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;


@Interceptor
public class LogInterceptor {
	
	@AroundInvoke
	public Object metodoInterceptador(InvocationContext ic) throws Exception{
		System.out.println("Antes de chamar o método: " + ic.getMethod());
		Object obj = ic.proceed();
		System.out.println("Após chamar o método: " + ic.getMethod());
		return obj;
		
	}
	
	

}
