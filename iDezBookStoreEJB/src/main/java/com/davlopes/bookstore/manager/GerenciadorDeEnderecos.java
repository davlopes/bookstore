package com.davlopes.bookstore.manager;

import java.util.List;

import com.davlopes.bookstore.models.Endereco;

public interface GerenciadorDeEnderecos {
	
	public void cadastrar(Endereco endereco);
	public void remover(Endereco endereco);
	public void alterar(Endereco endereco);
	public List<Endereco> obterTodos();

}
