package com.davlopes.bookstore.manager;

import java.util.List;

import com.davlopes.bookstore.models.Livro;

public interface GerenciadorDeLivros {
	
	public void cadastrar(Livro livro);
	public void remover(Livro livro);
	public void alterar(Livro livro);
	public List<Livro> obterTodos();
	public List<Livro> obterLancamentos();
}
