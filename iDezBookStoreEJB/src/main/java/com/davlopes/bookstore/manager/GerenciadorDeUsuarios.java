package com.davlopes.bookstore.manager;

import java.util.List;

import com.davlopes.bookstore.models.Usuario;

public interface GerenciadorDeUsuarios {
	
	public void cadastrar(Usuario usuario);
	public void remover(Usuario usuario);
	public void alterar(Usuario usuario);
	public List<Usuario> obterTodos();

}
