package com.davlopes.bookstore.manager;

import java.util.List;

import com.davlopes.bookstore.models.Categoria;

public interface GerenciadorDeCategorias {
	
	public void cadastrar(Categoria categoria);
	public void remover(Categoria categoria);
	public void alterar(Categoria categoria);
	public List<Categoria> obterTodos();

}
