package com.davlopes.bookstore.beans;

import java.util.Properties;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Singleton
@Startup
public class MailSender {

	private String username = "idez.dam@gmail.com";
	private String senha = "ninguempassa";

	@Resource(mappedName="java:jboss/mail/Default")
	private Session mailSession;


	@Asynchronous
	public void enviarMsg(String destino, String assunto, String conteudo) {

		try {

			Message msg = new MimeMessage(mailSession);
			msg.setFrom(new InternetAddress(username));
			msg.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(destino));
			msg.setSubject(assunto);
			msg.setText(conteudo);
			Transport.send(msg);

		} catch (MessagingException e) {
			System.out.println("EMAIL NÃO ENVIADO" + e);
		}

	}

}
