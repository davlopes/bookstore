package com.davlopes.bookstore.beans;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.davlopes.bookstore.interceptors.LogInterceptor;
import com.davlopes.bookstore.manager.GerenciadorDeLivros;
import com.davlopes.bookstore.models.Livro;

@Stateless
@Local(GerenciadorDeLivros.class)
@Interceptors({LogInterceptor.class})
public class GerenciadorDeLivrosBean implements GerenciadorDeLivros {
	@PersistenceContext
	private EntityManager em;
	
	public void cadastrar(Livro livro){
		this.em.persist(livro);
	}
	
	public void remover(Livro livro){
		Livro gerenciado = this.em.find(Livro.class,livro.getId());
		this.em.remove(gerenciado);
	}
	
	public void alterar(Livro Livro){
		this.em.merge(Livro);
	}
	
	@SuppressWarnings("unchecked")
	public List<Livro> obterTodos(){
		Query q = this.em.createQuery("SELECT l FROM Livro l");
		return q.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Livro> obterLancamentos(){
		Query q = this.em.createQuery("SELECT l FROM  Livro l ORDER BY l.id DESC LIMIT 5");		
		return q.getResultList();		
	}
	
}
