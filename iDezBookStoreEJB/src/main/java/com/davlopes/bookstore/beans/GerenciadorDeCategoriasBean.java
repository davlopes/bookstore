package com.davlopes.bookstore.beans;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.davlopes.bookstore.interceptors.LogInterceptor;
import com.davlopes.bookstore.manager.GerenciadorDeCategorias;
import com.davlopes.bookstore.models.Categoria;

@Stateless
@Local(GerenciadorDeCategorias.class)
@Interceptors({LogInterceptor.class})
public class GerenciadorDeCategoriasBean implements GerenciadorDeCategorias {
	
	@PersistenceContext
	private EntityManager em;
	
	public void cadastrar(Categoria categoria){
		this.em.persist(categoria);
	}
	
	public void remover(Categoria categoria){	
		Categoria gerenciada = em.find(Categoria.class, categoria.getId());
		this.em.remove(gerenciada);
	}
	
	public void alterar(Categoria categoria){
		this.em.merge(categoria);
	}
	
	@SuppressWarnings("unchecked")
	public List<Categoria> obterTodos(){
		Query q = this.em.createQuery("SELECT c FROM Categoria c");
		return q.getResultList();
	}
	
}
