package com.davlopes.bookstore.beans;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicSession;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.swing.text.AbstractDocument.Content;

import com.davlopes.bookstore.beans.MailSender;
import com.davlopes.bookstore.manager.GerenciadorDeUsuarios;
import com.davlopes.bookstore.models.Usuario;

@Startup
@Singleton
public class BookStoreTime {

	@PostConstruct
	public void depoisDaCriacaoDoTimer() {

		System.out.println("O Timer foi criado !!!!!!!");

	}

	@Schedule(second = "*/60", minute = "*/1", hour = "*", persistent = false)
	public void Execute() {

		System.out.println("TIME Funcionando...");

	}

}
