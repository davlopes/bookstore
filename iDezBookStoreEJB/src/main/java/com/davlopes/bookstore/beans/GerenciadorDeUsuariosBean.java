package com.davlopes.bookstore.beans;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.davlopes.bookstore.interceptors.LogInterceptor;
import com.davlopes.bookstore.manager.GerenciadorDeUsuarios;
import com.davlopes.bookstore.models.Usuario;
import com.davlopes.bookstore.beans.BookStoreTime;

@Stateless
@Local(GerenciadorDeUsuarios.class)
@Interceptors({ LogInterceptor.class })
public class GerenciadorDeUsuariosBean implements GerenciadorDeUsuarios {

	@Resource(mappedName = "java:/JmsXA")
	QueueConnectionFactory factory;

	@PersistenceContext
	private EntityManager em;
	
	
	@EJB
	MailSender mailSender;
	
	public void cadastrar(Usuario usuario) {

		try {
			em.persist(usuario);
			mailSender.enviarMsg(usuario.getEmail(),
					"Cadastro Efetuado Com Sucesso", usuario.toString());

		} catch (Exception e) {
			System.out.println("Estourou a exeção...  " + e.getMessage());
			e.printStackTrace();

		}

	}

	public void remover(Usuario usuario) {
		Usuario gerenciado = this.em.find(Usuario.class, usuario.getId());
		this.em.remove(gerenciado);
	}

	public void alterar(Usuario usuario) {
		this.em.merge(usuario);
	}

	@SuppressWarnings("unchecked")
	public List<Usuario> obterTodos() {
		Query q = this.em.createQuery("SELECT u FROM Usuario u");
		return q.getResultList();
	}

}
